<?php
# Establish connection to MySQL database
$servername = "localhost";
$username = "sw_user";
$password = "sw_user_pswd";
$dbname = "sw_devtest";

$conn = new mysqli($servername, $username, $password, $dbname);

# Check connection
if ($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}

# Query for orderid and comments
$sql = "SELECT orderid, comments FROM sweetwater_test";
$result = mysqli_query($conn, $sql);

# Prepare statement to save shipdates
$stmt = $conn->prepare("
	UPDATE sweetwater_test
	SET shipdate_expected = ?
	WHERE sweetwater_test.orderid = ?
");
$stmt->bind_param("si", $shipdate, $orderid);

# Create arrays for comment groups
$signature_comments = [];
$call_comments = [];
$candy_comments = [];
$referral_comments = [];
$misc_comments = [];

# For each comment in result...
if (mysqli_num_rows($result) > 0){
	while($row = mysqli_fetch_assoc($result)){
		### Find and update expected shipdates in DB
		# Separate out expected shipdate
		$pieces = explode("Expected Ship Date:", $row["comments"]);

		# If found, set expected shipdate
		if (count($pieces) > 1){
			# Format date
			$time = strtotime($pieces[1]);
			$date = date('Y-m-d', $time);

			# Update DB with shipdate - set parameters and execute
			$orderid = $row["orderid"];
			$shipdate = $date;
			$stmt->execute();
		}

		### Search for keywords in comment to categorize
		if ( preg_match("/signature/i", $pieces[0]) ){
			# Signature
			array_push($signature_comments, $pieces[0]);
		} elseif ( preg_match("/call|llámame|comunicarse /i", $pieces[0]) ){
			# Call / Don't Call
			array_push($call_comments, $pieces[0]);
		} elseif ( preg_match("/candy|tootsie|smarties|taffy/i", $pieces[0]) ){
			# Candy
			array_push($candy_comments, $pieces[0]);
		} elseif ( preg_match("/referred|referral|heard of|heard about|told me about|fan|friend |internet/i", $pieces[0]) ){
			# Referrals
			array_push($referral_comments, $pieces[0]);
		} else {
			# Miscellaneous
			array_push($misc_comments, $pieces[0]);
		}
	}

} else {
	echo "0 results";
}

$stmt->close();
$conn->close();
?>


<!-- LET THE (MOSTLY) HTML BEGIN -->
<html>
<body>
	<table>
		<tr>
			<th>Candy Comments</th>
		</tr>
		<?php
			foreach ($candy_comments as $comment){
				echo "<tr>
					<td>" . $comment . "</td>
				</tr>" ;
			}
		?>
	</table>
	<br>
	<table>
		<tr>
			<th>Call Instruction Comments</th>
		</tr>
		<?php
			foreach ($call_comments as $comment){
				echo "<tr>
					<td>" . $comment . "</td>
				</tr>" ;
			}
		?>
	</table>
	<br>
	<table>
		<tr>
			<th>Referral Comments</th>
		</tr>
		<?php
			foreach ($referral_comments as $comment){
				echo "<tr>
					<td>" . $comment . "</td>
				</tr>" ;
			}
		?>
	</table>
	<br>
	<table>
		<tr>
			<th>Signature Requirement Comments</th>
		</tr>
		<?php
			foreach ($signature_comments as $comment){
				echo "<tr>
					<td>" . $comment . "</td>
				</tr>" ;
			}
		?>
	</table>
	<br>
	<table>
		<tr>
			<th>Miscellaneous Comments</th>
		</tr>
		<?php
			foreach ($misc_comments as $comment){
				echo "<tr>
					<td>" . $comment . "</td>
				</tr>" ;
			}
		?>
	</table>
</body>
</html>