Hi! Here is my solution to this coding challenge!

A few thoughts I had during this exercise:

In lines 19-25 and 60-63, I used a prepared statement since the update query
is repeated with parameters changing to reduce parsing time AND prepared
statements protect from SQL injection.

I thought about writing multiple SQL queries using keywords for the different
types of comments, but I already have the strings from querying them all in
line 16. Additionally, that approach would miss the comments that would be
grouped as miscellaneous, which don't include any of the used keywords. While
I could write another SQL query for those, grabbing all of the entries and
sorting each comment into one category one time while looping through them
leaves me less likely to miss a comment or count a comment twice.


8/31/2022 Update:
After updating my code to use regex, I tried to quickly find a way to
translate comments written in Spanish to English, but I didn't have a lot
of luck. I noticed Laravel's Localization environment does that though!
For the sake of time, I included the two Spanish words that translate to
"call me" in the regex string for this solution.

I also thought about using regex to find names but exclude the salespeople
to catch more referral comments. I can deduce a few names (Nick, Aaron/Aron)
from a selection of comments, but that search might accidentally include
comments that have nothing to do with referrals and it exclude referrals from
people with the same names as the salespeople.


Thank you again for this opportunity to interview for Sweetwater.

Samantha Vuskalns